#![allow(non_snake_case)]

use dioxus::prelude::*;
use hoodie::{use_const_context, use_const_context_provider};

#[test]
fn test_context_received() {
    let mut vdom = VirtualDom::new(Provider);
    let _ = vdom.rebuild();

    let text = dioxus_ssr::render(&vdom);
    assert_eq!(text, "<div></div>");
}

#[test]
fn test_no_context_received() {
    let mut vdom = VirtualDom::new(NoContextAsserter);
    let _ = vdom.rebuild();

    let text = dioxus_ssr::render(&vdom);
    assert_eq!(text, "<div></div>");
}

// not clone
struct Data(i64);

pub fn Provider(cx: Scope) -> Element {
    let data = use_const_context_provider(&cx, || Data(42));
    assert!(matches!(data, &Data(42)));

    cx.render(rsx!(ReceiptAsserter {}))
}

pub fn ReceiptAsserter(cx: Scope) -> Element {
    let data = use_const_context::<Data>(&cx);
    assert!(matches!(data, Some(&Data(42))));

    cx.render(rsx!(div {}))
}

pub fn NoContextAsserter(cx: Scope) -> Element {
    let data = use_const_context::<Data>(&cx);
    assert!(matches!(data, None));

    cx.render(rsx!(div {}))
}
