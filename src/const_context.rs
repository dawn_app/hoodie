use dioxus::prelude::*;
use std::borrow::Borrow;
use std::rc::Rc;

/// Internal struct for tracking a const context
struct ConstContext<T>(Rc<T>);

impl<T> Clone for ConstContext<T> {
    fn clone(&self) -> Self {
        ConstContext(self.0.clone())
    }
}

impl<T> ConstContext<T> {
    pub fn new(x: T) -> Self {
        Self(Rc::new(x))
    }
}

/// Provide an immutable value to all descendant components
///
/// Use [`use_const_context`] in descendant components to access it
pub fn use_const_context_provider<T: 'static>(cx: &ScopeState, value: impl FnOnce() -> T) -> &T {
    let context = cx.use_hook(|| cx.provide_context::<ConstContext<T>>(ConstContext::new(value())));
    &*context.0
}

/// Consume an immutable context provided by an ancestor component through [`use_const_context_provider`]
pub fn use_const_context<T: 'static>(cx: &ScopeState) -> Option<&T> {
    let context = cx.use_hook(|| cx.consume_context::<ConstContext<T>>());
    context.as_ref().map(|value| value.0.borrow())
}
